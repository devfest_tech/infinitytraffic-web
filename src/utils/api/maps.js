import { URL } from '../helper'
export default {
  async getPositionOnTheMap(axios, data){
    return axios.get(URL + `/apiNodes/${data}`)
  }
}