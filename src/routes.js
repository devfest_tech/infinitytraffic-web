// Define Route Components
import Home from './views/Home'
import Statistic from './views/Statistic'
export const routes = [
    { path: '/', name: 'homepage', component: Home },
    { path: '/statistic', name: 'statistic', component: Statistic }
]