import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import ViewRouter from 'vue-router'
import VueRouter from 'vue-router'
import { routes } from './routes'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '../src/assets/css/general.scss'
import axios from 'axios'
import rootStore from './store/index.js'
import 'es6-promise/auto'

Vue.use(Vuex)
const store = new Vuex.Store(rootStore);
Vue.prototype.$http = axios
Vue.use(Vuetify)
Vue.use(ViewRouter)

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAM9WmYBydj2uLum2yC4VIGHSvCRRd0mP4',
    libraries: 'places'
  }
})

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
