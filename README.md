# google-dev-traffic

> This is a project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).


# Chức năng hiển thị tình trạng ùn tắc tại các nút giao thông

![alt text](./src/assets/images/m1.png)

# Màn hiển thị thống kê các nút giao thông (Chức năng chưa hoàn thiện)

![alt text](./src/assets/images/m2.png)